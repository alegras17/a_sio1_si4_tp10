package exercices;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import static utilitaires.UtilDate.*;

public class Q03 {

    public static void main(String[] args) throws SQLException {
        
        Connection cxBdd =    connexionBdd() ;
      
        String requete= "Select avg(poids) as poidsMoyen from Personne where sexe='M'";
        
        Statement   cmde   = cxBdd.createStatement();
        ResultSet   pers   = cmde.executeQuery(requete);
   
      
        if(  pers.next() ) {
            
           System.out.printf("\n Moyenne des poids des judokas masculins : %2d kg\n",
  
                              pers.getInt("poidsMoyen")
                            );
        }
        
        System.out.println("\n");
        pers.close();
        cxBdd.close();
    }

    private static void afficher(ResultSet pers) throws SQLException {
        
        final String format=" %-15s %-15s %-10s %4d kg %-15s %2d victoires\n";
        System.out.printf(format,
                                      
                          pers.getString("nom"),
                          pers.getString("prenom"),
                          dateVersChaine(pers.getDate("datenaiss")),
                          pers.getInt("poids"),
                          pers.getString("ville"),
                          pers.getInt("nbVictoires"));
    }

    private static Connection connexionBdd() throws SQLException {
 
        final String    URL                  = "jdbc:derby:BddJudo";
        final String    UTILISATEUR          = "uJudo";
        final String    MOTDEPASSE           = "judo";
        
        DriverManager.registerDriver(new org.apache.derby.jdbc.EmbeddedDriver());
        Connection cxBdd =    DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE) ;
        return cxBdd;
    }    
}