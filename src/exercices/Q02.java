package exercices;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.Scanner;
import static utilitaires.UtilDate.*;

public class Q02 {

    public static void main(String[] args) throws SQLException {
        
        Scanner clavier = new Scanner(System.in);
        Connection cxBdd =    connexionBdd() ;
        
        String ville = null;
        System.out.println("Saisir la ville souhaitée");
        ville = clavier.next();
        System.out.println("");
        String requete= "Select nom, prenom, datenaiss, nbvictoires " +
                        "From   Personne "    +
                        "where ville='"+ville+"'";
        
        Statement   cmde   = cxBdd.createStatement();
        ResultSet   pers   = cmde.executeQuery(requete);
   
        while (  pers.next() ) {
            
            afficher(pers);
         }
        
        System.out.println("\n");
        pers.close();
        cxBdd.close();
    }

    private static void afficher(ResultSet pers) throws SQLException {
        
        final String format=" %-15s %-15s %4d ans %2d victoires\n";
        System.out.printf(format,
                                      
                          pers.getString("nom"),
                          pers.getString("prenom"),
                          ageEnAnnees(pers.getDate("datenaiss")),
                          pers.getInt("nbVictoires"));
    }

    private static Connection connexionBdd() throws SQLException {
 
        final String    URL                  = "jdbc:derby:BddJudo";
        final String    UTILISATEUR          = "uJudo";
        final String    MOTDEPASSE           = "judo";
        
        DriverManager.registerDriver(new org.apache.derby.jdbc.EmbeddedDriver());
        Connection cxBdd =    DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE) ;
        return cxBdd;
    }    
}