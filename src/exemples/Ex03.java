package exemples;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Ex03 {

    public static void main(String[] args) throws SQLException {
        
        Connection cxBdd =    connexionBdd() ;
        
        String requete= "Select    sexe, count(*) as  effectif " +
                        "From      Personne                    " +
                        "Group  by sexe                        " ;
        
        Statement   cmde   = cxBdd.createStatement();
        ResultSet   pers   = cmde.executeQuery(requete);
   
        System.out.println("\nStatistique par genres:\n\n");
         
        while (  pers.next() ) {
            
            afficher(pers);
         }
        
        System.out.println("\n");
        pers.close();
        cxBdd.close();
    }

    private static void afficher(ResultSet pers) throws SQLException {
 
        System.out.printf(" %-6s %3d\n",
                                      
                          pers.getString("sexe").equals("F") ?"Femmes":"Hommes",
                          pers.getInt("effectif")
                         );
    }

    private static Connection connexionBdd() throws SQLException {
 
        final String    URL                  = "jdbc:derby:BddJudo";
        final String    UTILISATEUR          = "uJudo";
        final String    MOTDEPASSE           = "judo";
        
        DriverManager.registerDriver(new org.apache.derby.jdbc.EmbeddedDriver());
        Connection cxBdd =    DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE) ;
        return cxBdd;
    }    
}