package exemples;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Ex04 {

    public static void main(String[] args) throws SQLException {
        
        Connection cxBdd =    connexionBdd() ;
      
        String requete= "Select max(poids) as poidsMax " +
                        "From   Personne               " ;                           
                        
        
        Statement   cmde   = cxBdd.createStatement();
        ResultSet   pers   = cmde.executeQuery(requete);
   
         
        if(  pers.next() ) {
            
           System.out.printf("\n Le poids le plus élevé: %4d kg\n",
  
                              pers.getInt("poidsMax")
                            );
        }
        
        System.out.println("\n");
        pers.close();
        cxBdd.close();
    }

    private static Connection connexionBdd() throws SQLException {
 
        final String    URL                  = "jdbc:derby:BddJudo";
        final String    UTILISATEUR          = "uJudo";
        final String    MOTDEPASSE           = "judo";
        
        DriverManager.registerDriver(new org.apache.derby.jdbc.EmbeddedDriver());
        Connection cxBdd =    DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE) ;
        return cxBdd;
    }    
}